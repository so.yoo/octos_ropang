﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Ropang
{
    public class MainView : MonoBehaviour
    {
        [SerializeField] private GameObject[] _views;

        [SerializeField] private Button _testOctosToThisTablet;

        [SerializeField] private AudioSource _openningSound;

        [SerializeField] private Button _product1MinusButton;
        [SerializeField] private Button _product1PlusButton;

        [SerializeField] private Button _product2MinusButton;
        [SerializeField] private Button _product2PlusButton;

        [SerializeField] private Text _product1CountText;
        [SerializeField] private Text _product2CountText;
        [SerializeField] private Text _product1SumPriceText;
        [SerializeField] private Text _product2SumPriceText;

        [SerializeField] private Button _orderButton;

        // View-2

        [SerializeField] private TextMeshProUGUI _p1CountText;
        [SerializeField] private TextMeshProUGUI _p1SumText;
        [SerializeField] private TextMeshProUGUI _p2CountText;
        [SerializeField] private TextMeshProUGUI _p2SumText;
        [SerializeField] private Text _totalPriceText;
        [SerializeField] private Button _insertCardOkButton;


        // View-3
        [SerializeField] private Button _completePurchaseButton;

        [SerializeField] private Button _requestOrderNumberButton;

        [SerializeField] private Button[] _dialButtons;

        private int _product1Count = 0;
        private int _product2Count = 0;

        private const int Product1Price = 1500;
        private const int Product2Price = 1500;

        StringBuilder _phoneNumberValue = new StringBuilder();
        [SerializeField] private Text _phoneNumberText;

        [SerializeField] private GameObject[] _footers;
        [SerializeField] private GameObject[] _footerSteps;

        private void OnEnable()
        {
            APP.MsgCenter.OnOrderMessage += OnOrderMessage;
        }

        private void OnDisable()
        {
            APP.MsgCenter.OnOrderMessage -= OnOrderMessage;
        }

        void Start()
        {
            _product1SumPriceText.text = "0";
            _product2SumPriceText.text = "0";

            BindViewControllers();

            foreach (var item in _views)
                item.SetActive(false);

            SetView(0);
        }

        // IEnumerator Start()
        // {
        //     yield return new WaitForSeconds(1F);
        //     
        //     var msg = new MsgOrder();
        //     OnOrderMessage(msg);
        // }

        void BindViewControllers()
        {
            _testOctosToThisTablet.onClick.AddListener(() =>
            {
                var msg = new MsgStartOrderProcess();

                APP.TempUdpAgentRef.SendMsg(msg);
            });

            _product1PlusButton.onClick.AddListener(() =>
            {
                ++_product1Count;
                _product1CountText.text = _product1Count.ToString();
                string format = "{0:#,##0.#############}";
                _product1SumPriceText.text = String.Format("{0:#,###}", _product1Count * Product1Price);
            });
            _product1MinusButton.onClick.AddListener(() =>
            {
                _product1Count = --_product1Count < 0 ? 0 : _product1Count;
                _product1CountText.text = _product1Count.ToString();
                _product1SumPriceText.text = string.Format("{0}", _product1Count * Product1Price);
            });

            _product2PlusButton.onClick.AddListener(() =>
            {
                ++_product2Count;
                _product2CountText.text = _product2Count.ToString();
                _product2SumPriceText.text = string.Format("{0}", _product2Count * Product2Price);
            });
            _product2MinusButton.onClick.AddListener(() =>
            {
                _product2Count = --_product2Count < 0 ? 0 : _product2Count;
                _product2CountText.text = _product2Count.ToString();
                _product2SumPriceText.text = string.Format("{0}", _product2Count * Product2Price);
            });

            _orderButton.onClick.AddListener(() =>
            {
                if (_product1Count > 0 || _product2Count > 0)
                    SetView(2);
            });

            _insertCardOkButton.onClick.AddListener(() => { SetView(3); });

            _completePurchaseButton.onClick.AddListener(() => { SetView(4); });

            _requestOrderNumberButton.onClick.AddListener(() =>
            {
                SetView(5);


                DOVirtual.DelayedCall(3F, () =>
                {
                    SetView(0);

                    var msg = new MsgOrderProcessEnded();
                    APP.Server.SendMsg(msg);
                });
            });

            for (int i = 0; i < _dialButtons.Length; ++i)
            {
                int i2 = i;
                _dialButtons[i].onClick.AddListener(() =>
                {
                    _phoneNumberValue.Append(i2.ToString());
                    if (_phoneNumberValue.Length == 4)
                        _phoneNumberValue.Append("-");

                    _phoneNumberText.text = _phoneNumberValue.ToString();
                });
            }
        }

        private void OnOrderMessage(MsgStartOrderProcess msgStart)
        {
            Debug.LogFormat("MainView OnOrderMessage ({0}) ", msgStart.msgName);


            SetView(1);
        }

        void SetView(int step)
        {
            foreach (var item in _footers)
                item.SetActive(false);
            foreach (var item in _footerSteps)
                item.SetActive(false);

            switch (step)
            {
                case 0:
                {
                    _views[5].SetActive(false);
                    _views[0].SetActive(true);

                    _product1Count = 0;
                    _product2Count = 0;

                    _footers[0].SetActive(true);
                    break;
                }
                case 1:
                {
                    _views[0].SetActive(false);
                    _views[1].SetActive(true);

                    _product1CountText.text = _product1Count.ToString();
                    _product2CountText.text = _product2Count.ToString();

                    _footers[1].SetActive(true);
                    _footerSteps[0].SetActive(true);

                    _openningSound.Play();
                    break;
                }
                case 2:
                {
                    _views[1].SetActive(false);
                    _views[2].SetActive(true);
                    UpdateOrderList();

                    _footers[1].SetActive(true);
                    _footerSteps[1].SetActive(true);
                    break;
                }
                case 3:
                {
                    _views[2].SetActive(false);
                    _views[3].SetActive(true);

                    _footers[1].SetActive(true);
                    _footerSteps[2].SetActive(true);
                    break;
                }
                case 4:
                {
                    _views[3].SetActive(false);
                    _views[4].SetActive(true);

                    _footers[1].SetActive(true);
                    _footerSteps[3].SetActive(true);
                    break;
                }
                case 5:
                {
                    _views[4].SetActive(false);
                    _views[5].SetActive(true);

                    _footers[0].SetActive(true);

                    _openningSound.Play();
                    break;
                }
            }
        }

        void UpdateOrderList()
        {
            int p1Sum = _product1Count * 1500;
            int p2Sum = _product2Count * 1500;

            _p1CountText.text = string.Format("X {0}개", _product1Count);
            _p2CountText.text = string.Format("X {0}개", _product2Count);
            _p1SumText.text = string.Format("{0:#,###}원", p1Sum);
            _p2SumText.text = string.Format("{0:#,###}원", p2Sum);
            _totalPriceText.text = string.Format("{0:#,###}원", p1Sum + p2Sum);
        }
    }
}