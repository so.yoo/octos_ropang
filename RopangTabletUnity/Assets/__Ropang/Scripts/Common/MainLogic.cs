using System;
using Jois;
using UnityEngine;

namespace Ropang
{
    public class MainLogic : MonoBehaviour
    {
        [SerializeField] private UdpServer _udpServer;
        [SerializeField] private UdpAgent _udpAgent;

        [SerializeField] private JTcpSocket _tcpClient;

        private void OnEnable()
        {
            APP.MsgCenter.OnServerGetMessage += OnServerGetMessage;
            APP.MsgCenter.OnServerAddressGot += OnServerAddressGot;
            _tcpClient._onRead += OnReadTcpMessage;
        }

        private void OnDisable()
        {
            APP.MsgCenter.OnServerGetMessage -= OnServerGetMessage;
            APP.MsgCenter.OnServerAddressGot -= OnServerAddressGot;
            _tcpClient._onRead -= OnReadTcpMessage;
        }

        void Start()
        {
            StartUpApp();
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
                Application.Quit();
        }

        void StartUpApp()
        {
            _tcpClient.enabled = false;
            Application.targetFrameRate = 10;


            //_udpServer.StartUp();
        }


        void OnServerGetMessage(string jsonMsg)
        {
            var msgBase = JsonUtility.FromJson<MsgBase>(jsonMsg);
            Debug.LogFormat("msg ({0}) ", msgBase.msgName);

            switch (msgBase.msgName)
            {
                case "test":
                {
                    var msg = JsonUtility.FromJson<MsgTest>(jsonMsg);
                    Debug.LogFormat("MainLogic - msg({0}) code({1}) ", msg.msgName, msg.code);
                    break;
                }
                case "order":
                {
                    var msg = JsonUtility.FromJson<MsgStartOrderProcess>(jsonMsg);
                    APP.MsgCenter.OnOrderMessage(msg);
                    break;
                }
            }
        }

        void OnServerAddressGot(string serverAddress)
        {
            Debug.LogFormat("MainLogic got server-addr({0}) ", serverAddress);
            // tcp_client.connect( serverAddress )

            // _tcpClient.Connect(serverAddress, 
            // //_tcpClient.Connect("localhost", //serverAddress, 
            //     8052,
            //     isSuccess =>
            //     {
            //         Debug.LogFormat("Tcp Connection is success ({0}) ", isSuccess);
            //     });
        }

        void OnReadTcpMessage(byte[] msg, int size)
        {
        }
    }
}