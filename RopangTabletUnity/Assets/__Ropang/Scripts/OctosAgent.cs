using UnityEngine;

namespace Ropang
{
    public class OctosAgent : MonoBehaviour
    {
        private void OnEnable()
        {
            APP.MsgCenter.OnServerAddressGot += OnServerAddressGot;
        }

        private void OnDisable()
        {
            APP.MsgCenter.OnServerAddressGot -= OnServerAddressGot;
        }


        void OnServerAddressGot(string serverAddress)
        {
            Debug.LogFormat("MainLogic got server-addr({0}) ", serverAddress);
            
            //TODO: 
        }
    }
}