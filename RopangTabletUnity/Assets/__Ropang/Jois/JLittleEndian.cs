﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public static class JLittleEndian
{
    // Write
    public static void Write_u8(byte b, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = b;
    }

    public static void Write_u16(ushort s, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = (byte) (0xff & s);
        bytes[offset++] = (byte) (0xff & (s >> 8));
    }

    public static void Write_u32(uint i32, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = (byte) (0xff & i32);
        bytes[offset++] = (byte) (0xff & (i32 >> 8));
        bytes[offset++] = (byte) (0xff & (i32 >> 16));
        bytes[offset++] = (byte) (0xff & (i32 >> 24));
    }

    public static void Write_i8(char b, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = (byte) b;
    }

    public static void Write_i16(short s, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = (byte) (0xff & s);
        bytes[offset++] = (byte) (0xff & (s >> 8));
    }

    public static void Write_i32(int i32, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = (byte) (0xff & i32);
        bytes[offset++] = (byte) (0xff & (i32 >> 8));
        bytes[offset++] = (byte) (0xff & (i32 >> 16));
        bytes[offset++] = (byte) (0xff & (i32 >> 24));
    }

    public static void Write_i64(long i64, ref byte[] bytes, ref int offset)
    {
        bytes[offset++] = (byte) (0xff & i64);
        bytes[offset++] = (byte) (0xff & (i64 >> 8));
        bytes[offset++] = (byte) (0xff & (i64 >> 16));
        bytes[offset++] = (byte) (0xff & (i64 >> 24));
        bytes[offset++] = (byte) (0xff & (i64 >> 32));
        bytes[offset++] = (byte) (0xff & (i64 >> 40));
        bytes[offset++] = (byte) (0xff & (i64 >> 48));
        bytes[offset++] = (byte) (0xff & (i64 >> 56));
    }

    public static void WriteDouble(double d, ref byte[] bytes, ref int offset)
    {
        Write_i64(System.BitConverter.DoubleToInt64Bits(d), ref bytes, ref offset);
    }

    // Read
    public static byte Read_u8(byte[] bytes, ref int offset)
    {
        byte ret = bytes[offset];
        ++offset;
        return ret;
    }

    public static ushort Read_u16(byte[] bytes, ref int offset)
    {
        ushort ret = (ushort) (
            ((bytes[offset + 0] & 0xff) << 0) |
            ((bytes[offset + 1] & 0xff) << 8)
        );
        offset += 2;
        return ret;
    }

    public static ushort Read_u16(byte[] bytes)
    {
        ushort ret = (ushort) (
            ((bytes[0] & 0xff) << 0) |
            ((bytes[1] & 0xff) << 8)
        );
        return ret;
    }

    public static uint Read_u32(byte[] bytes)
    {
        uint ret = (uint) (
            ((bytes[0] & 0xff) << 0) |
            ((bytes[1] & 0xff) << 8) |
            ((bytes[2] & 0xff) << 16) |
            ((bytes[3] & 0xff) << 24)
        );
        return ret;
    }

    public static uint Read_u32(byte[] bytes, ref int offset)
    {
        uint ret = (uint) (
            ((bytes[offset + 0] & 0xff) << 0) |
            ((bytes[offset + 1] & 0xff) << 8) |
            ((bytes[offset + 2] & 0xff) << 16) |
            ((bytes[offset + 3] & 0xff) << 24)
        );
        offset += 4;
        return ret;
    }

    public static char Read_i8(byte[] bytes, ref int offset)
    {
        char ret = (char) bytes[offset]; //??????
        ++offset;
        return ret;
    }

    public static short Read_i16(byte[] bytes, ref int offset)
    {
        short ret = (short) (
            ((bytes[offset + 0] & 0xff) << 0) |
            ((bytes[offset + 1] & 0xff) << 8)
        );
        offset += 2;
        return ret;
    }

    public static int Read_i32(byte[] bytes, ref int offset)
    {
        int ret = (int) (
            ((bytes[offset + 0] & 0xff) << 0) |
            ((bytes[offset + 1] & 0xff) << 8) |
            ((bytes[offset + 2] & 0xff) << 16) |
            ((bytes[offset + 3] & 0xff) << 24)
        );
        offset += 4;
        return ret;
    }

    public static long Read_i64(byte[] bytes, ref int offset)
    {
        unchecked
        {
#pragma warning disable 675
            long ret = (long) (
                ((long) (bytes[offset + 0] & 0xff) << 0) |
                ((long) (bytes[offset + 1] & 0xff) << 8) |
                ((long) (bytes[offset + 2] & 0xff) << 16) |
                ((long) (bytes[offset + 3] & 0xff) << 24) |
                ((long) (bytes[offset + 4] & 0xff) << 32) |
                ((long) (bytes[offset + 5] & 0xff) << 40) |
                ((long) (bytes[offset + 6] & 0xff) << 48) |
                ((long) (bytes[offset + 7] & 0xff) << 56)
            );
#pragma warning restore 675
            offset += 8;
            return ret;
        }
    }

    public static double ReadDouble(byte[] bytes, ref int offset)
    {
        return System.BitConverter.Int64BitsToDouble(Read_i64(bytes, ref offset));
    }

    public static void Test_BitConvert()
    {
        char data0 = (char) 1;
        short data1 = 2;
        int data2 = 3;
        long data3 = 4;

        byte[] bytes = new byte[1024];
        int offset = 0;
        Write_i8(data0, ref bytes, ref offset);
        Write_i16(data1, ref bytes, ref offset);
        Write_i32(data2, ref bytes, ref offset);
        Write_i64(data3, ref bytes, ref offset);

        Assert.IsTrue(offset == 15);

        offset = 0;
        data0 = Read_i8(bytes, ref offset);
        data1 = Read_i16(bytes, ref offset);
        data2 = Read_i32(bytes, ref offset);
        data3 = Read_i64(bytes, ref offset);

        Assert.IsTrue(data0 == 1);
        Assert.IsTrue(data1 == 2);
        Assert.IsTrue(data2 == 3);
        Assert.IsTrue(data3 == 4);


        Debug.LogFormat("[Test] BitConvert succeeded.");
    }
}